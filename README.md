# API Sample

## Setting dev environment

Run the following command to run Mysql, PHP MyAdmin and an Apache web server:
```shell
docker-compose up -d
```

Then, enter `api_sample_www` container by running this command:
```shell
docker exec -it api_sample_www bash
```

And install `Composer` dependencies:
```shell
composer install
```
