<?php

namespace App\Tests\Repository;

use App\Entity\Category;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Exception\ORMException;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CategoryRepositoryTest extends KernelTestCase
{
    private ?EntityManager $manager;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->manager = $kernel->getContainer()
            ->get("doctrine")
            ->getManager();
    }

    protected function tearDown(): void
    {
        $purger = new ORMPurger($this->manager);
        $purger->purge();

        parent::tearDown();

        $this->manager->close();
        $this->manager = null;
    }

    /**
     * @throws ORMException
     */
    public function testAddCategory(): void
    {
        $name = "Test";
        $description = "Description";

        $category = new Category();
        $category->setName($name)->setDescription($description);
        $this->manager->persist($category);
        $this->manager->flush();

        $repository = $this->manager->getRepository(Category::class);
        $categories = $repository->findAll();

        $testedCategory = $categories[0];

        self::assertEquals($category->getId(), $testedCategory->getId());
        self::assertEquals($category->getName(), $testedCategory->getName());
        self::assertEquals($category->getDescription(), $testedCategory->getDescription());
        self::assertEquals($category->getProducts(), $testedCategory->getProducts());
    }
}
