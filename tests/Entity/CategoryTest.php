<?php

namespace App\Tests\Entity;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;

class CategoryTest extends ApiTestCase
{
    public function testSomething(): void
    {
        $path = '/categories';

        $response = static::createClient()->request('GET', $path);

        $this->assertResponseIsSuccessful();
        $this->assertJsonContains(['@id' => $path]);
    }
}
