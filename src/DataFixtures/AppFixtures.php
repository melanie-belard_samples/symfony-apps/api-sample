<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    /**
     * @var Category[]
     */
    private array $categories = [];

    public function load(ObjectManager $manager): void
    {
        for ($c = 1; $c <= 3; $c++) {
            $category = $this->createCategory($c);

            $this->categories[] = $category;
            $manager->persist($category);
        }

        for ($p = 1; $p <= 24; $p++) {
            $product = $this->createProduct($p);

            $manager->persist($product);
        }

        $manager->flush();
    }

    /**
     * @param int $number
     * @return Category
     */
    private function createCategory(int $number): Category
    {
        $category = new Category();
        $category->setName("Category #$number")
            ->setDescription("This is the category number $number!");

        return $category;
    }

    /**
     * @param int $number
     * @param Category|null $category
     * @return Product
     */
    private function createProduct(int $number, ?Category $category = null): Product
    {
        $product = new Product();

        $product->setName("Product #$number")
            ->setCategory(
                $category ?:
                    $this->categories[mt_rand(0, count($this->categories) - 1)]
            )
            ->setDescription("This is the product number $number!");
        return $product;
    }
}
